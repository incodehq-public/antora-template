= Add Docs to an Existing Project
include::_attributes.adoc[]

On this page you'll learn:

* [x] the steps needed to update an existing project with Antora template.

See also:

* how to further xref:how-to/customise.adoc[customise] your repo.


== Add the Files

This repo provides a bunch of files that you can copy into your repo and then update:

* Clone the Repo
+
Start by cloning this repo to a temporary directory:
+
[source,powershell]
----
mkdir -p /tmp/antora-template
git clone https://gitlab.com/incodehq-public/antora-template /tmp/antora-template
----

* now copy over all the files/directories (excluding the `.git` directory) into your project repo.
+
[source,powershell]
----
git archive HEAD --remote=file:///tmp/antora-template --format tar | tar xvk
----
+
The `k` option prevents any files that already exist -- for example `.gitignore`, `README.adoc` or `pom.xml` -- from being overwritten.
For these files, manually diff and merge their contents instead.

* Make the changes suggested in the table below:
+
.Checklist
[cols="2l,2l,2a", options="header",stripes="none"]
|===

| Directory/File
| Contents/Key
| Details/notes

.3+l|
_docs/
  modules/
    ROOT/
      pages/
l|about.adoc
|Describe your project

l|
how-to/
a|Either delete, or update its content with your project's own "how-tos".

l|nav.adoc
a|Update to `xref:` pages as required

l|_docs/
  modules/
l|module-template
a|No changes required; keep as a template for new doc modules

.3+l|
_docs/
  antora.yml
|name
a| Used in the URL and fully qualified `link:` references

|title
a| As rendered in the UI

|nav
a|Comment out `module-template`.


l|_supplemental-ui/
  css/

l|site-custom.css
|Add custom styling.

l|_supplemental-ui/
  partials/

l|`*.hbs`
|Add other header menus of footer items for this repo, as required.
You can delete files that are empty.

.6+l|site.yml
site.gitlab-ci-template.yml
l|
site
  title
a|Update as appropriate

l|
site
  url
a| Set to the location of the deployed website.

(For `site.gitlab-ci-template.yml` only).

l|
site
  start_page
a|Update as appropriate

l|
content
  sources[0]
    url
a| Update the URL to refer to the git repo.

Optionally, you can add additional sources to create a single website that contains content from multiple git repos.

(For `site.gitlab-ci-template.yml` only).


l|
asciidoc
  attributes
    page-gitlab-owner
a|If the git repo is hosted on gitlab, then specify the owning group, eg `incodehq-private` or `incodehq-public`.

For example, this template repo is owned by "`incode-private`" (group https://gitlab.com/incodehq-private[]).

l|
asciidoc
  attributes
    page-gitlab-repo
a|If the git repo is hosted on gitlab, then specify the repo name.

For example, this template repo is "`antora-template`" (https://gitlab.com/incodehq-private/antora-template[]).

l|
_docs/
  gae/
    app.yaml
l|service
|Prefix for the generated website.

Must be unique across all sites.
Typically, same as `page-gitlab-repo`, but without any hyphens (hyphens are not allowed).

l|.gitignore
a|
a| Ensure that `_docs/gae/public/` is ignored.

.2+l|
pom.xml

|<groupId>
| Same as `page-gitlab-owner` attribute.

|<artifactId>
a|Same as `page-gitlab-repo` attribute

(optional - skip if the repo already has a `pom.xml`)

l|README.adoc
a|
a|Update the description.

This provides a top-level README for the repo when viewed from the gitlab.
The text should probably be similar to that of the `about.adoc` file.

|===


* Add and commit the changes:
+
[source]
----
git commit -am "adds Antora docs (copied from template)"
----
+
[WARNING]
====
Make sure you have committed changes before attempting to preview the site.
If you forget, the preview will most likely fail to generate any site.
====

* Preview the site:
+
[source,bash]
----
sh preview.sh
----

== Mirror repo to gitlab

Some of our git repos are on bitbucket (private/proprietary), others on github (public/open source).

To generate documentation, these need to be mirroed to gitlab, because it is a gitlab CI pipeline that builds and deploys the docs.

The easiest way to do this is simply to set up an additional remote to gitlab, and then push to this mirror whenever you want to update the published docs.

* private repos should belong in the `incodehq-private` group
* public repos should belong in the `incodehq-public` group
+
[NOTE]
====
We might choose to standardise on gitlab for everything in the future.
====

You can either use the `git remote add` command, or simply edit `.git/config`, adapting the existing `origin` repo as necessary.

For example, Estatio ECP's `.git/config` should look like:

[source]
.Estatio ECP's `.git/config`
----
...
remote "origin"]
    url = https://bitbucket.org/incodehq/estatio-ecp.git
    fetch = +refs/heads/*:refs/remotes/origin/*
[remote "gitlab"]
    url = https://gitlab.com/incodehq-private/estatio-ecp.git
    fetch = +refs/heads/*:refs/remotes/gitlab/*
...
----

Note also that there's no need to create a repo in gitlab explicitly; simply push to the new remote and a repo is created automatically:

[source,bash]
----
git checkout master
git push gitlab master
----


== Set up gitlab CI

The template includes a `.gitlab-ci.yml`.
Provided you haven't changed the location of the template files (`_docs` and `_supplemental-ui`) then there should be no reason to change the `.gitlab-ci.yml` - it is pure boilerplate).

However, on gitlab itself you will need to set up a secret environment variable to provide the credentials to push to GCP (where the docs are hosted).

Several environment variables are required:

* `GCP_DOC_APPCFG_OAUTH_TOKEN`
* `GCP_DOC_PROJECT_ID`
* `GITLAB_ANTORA_PERSONAL_ACCESS_TOKEN`

Easiest is to copy these values from an existing project, eg https://gitlab.com/incodehq-private/incode-docs/settings/ci_cd[https://gitlab.com/incodehq-private/incode-docs].
