module.exports = function (registry) {
  registry.blockMacro(function () {
    var self = this
    self.named('jira')
    self.process(function (parent, target, attrs) {
      var result = `link:https://incodehq.atlassian.net/browse/${target}[${target}]`
      return self.createBlock(parent, 'paragraph', result)
    })
  })
}
